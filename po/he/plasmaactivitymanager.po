# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Elkana Bardugo <ttv200@gmail.com>, 2016.
# Elkana Bardugo <ttv200@gmail.com>, 2017. #zanata
msgid ""
msgstr ""
"Project-Id-Version: plasmaactivitymanager\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-08 00:48+0000\n"
"PO-Revision-Date: 2017-05-16 06:57-0400\n"
"Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>\n"
"Language-Team: Hebrew <kde-i18n-doc@kde.org>\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Zanata 3.9.6\n"

#: sortedactivitiesmodel.cpp:311
#, kde-format
msgid "Used some time ago"
msgstr "שומש לפני כמה זמן"

#: sortedactivitiesmodel.cpp:327
#, kde-format
msgid "Used more than a year ago"
msgstr "שומש לפני שנה"

#: sortedactivitiesmodel.cpp:328
#, kde-format
msgctxt "amount in months"
msgid "Used a month ago"
msgid_plural "Used %1 months ago"
msgstr[0] "שומש לפי חודש"
msgstr[1] "שומש לפני %1 חודשים"

#: sortedactivitiesmodel.cpp:329
#, kde-format
msgctxt "amount in days"
msgid "Used a day ago"
msgid_plural "Used %1 days ago"
msgstr[0] "שומש לפני יום"
msgstr[1] "שומש לפני %1 ימים"

#: sortedactivitiesmodel.cpp:330
#, kde-format
msgctxt "amount in hours"
msgid "Used an hour ago"
msgid_plural "Used %1 hours ago"
msgstr[0] "שומש לפני שעה"
msgstr[1] "שומש לפני %1 שעות"

#: sortedactivitiesmodel.cpp:331
#, kde-format
msgctxt "amount in minutes"
msgid "Used a minute ago"
msgid_plural "Used %1 minutes ago"
msgstr[0] "שומש לפני דקה"
msgstr[1] "שומש לפני %1 דקות"

#: sortedactivitiesmodel.cpp:332
#, kde-format
msgid "Used a moment ago"
msgstr "היה בשימוש לפני כמה שניות"

#: switcherbackend.cpp:167
#, kde-format
msgid "Walk through activities"
msgstr "עבור בין פעילויות"

#: switcherbackend.cpp:172
#, kde-format
msgid "Walk through activities (Reverse)"
msgstr "עבור בין פעילויות (הפוך)"
