# Albanian translation for kdebase
# Copyright (c) 2009 Rosetta Contributors and Canonical Ltd 2009
# This file is distributed under the same license as the kdebase package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: kdebase\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-30 01:57+0000\n"
"PO-Revision-Date: 2009-06-28 20:36+0000\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: Albanian <sq@li.org>\n"
"Language: sq\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Launchpad-Export-Date: 2011-04-22 14:37+0000\n"
"X-Generator: Launchpad (build 12883)\n"

#: package/contents/config/config.qml:16
#, fuzzy, kde-format
#| msgctxt ""
#| "Title of the page that lets the user choose which location should the "
#| "folderview show"
#| msgid "Location"
msgid "Location"
msgstr "Vendndodhja"

#: package/contents/config/config.qml:23
#: package/contents/ui/FolderViewLayer.qml:393
#, kde-format
msgid "Icons"
msgstr "Ikonat"

#: package/contents/config/config.qml:30
#, fuzzy, kde-format
#| msgctxt ""
#| "Title of the page that lets the user choose how to filter the folderview "
#| "contents"
#| msgid "Filter"
msgid "Filter"
msgstr "Filtri"

#: package/contents/ui/BackButtonItem.qml:104
#, kde-format
msgid "Back"
msgstr ""

#: package/contents/ui/ConfigFilter.qml:63
#, kde-format
msgid "Files:"
msgstr ""

#: package/contents/ui/ConfigFilter.qml:64
#, fuzzy, kde-format
#| msgid "Show All Files"
msgid "Show all"
msgstr "Trego Tërë Skedat"

#: package/contents/ui/ConfigFilter.qml:64
#, fuzzy, kde-format
#| msgid "Show All Files"
msgid "Show matching"
msgstr "Trego Tërë Skedat"

#: package/contents/ui/ConfigFilter.qml:64
#, fuzzy, kde-format
#| msgid "Show All Files"
msgid "Hide matching"
msgstr "Trego Tërë Skedat"

#: package/contents/ui/ConfigFilter.qml:69
#, kde-format
msgid "File name pattern:"
msgstr ""

#: package/contents/ui/ConfigFilter.qml:76
#, kde-format
msgid "File types:"
msgstr ""

#: package/contents/ui/ConfigFilter.qml:82
#, fuzzy, kde-format
#| msgid "Show All Files"
msgid "Show hidden files:"
msgstr "Trego Tërë Skedat"

#: package/contents/ui/ConfigFilter.qml:130
#, fuzzy, kde-format
#| msgctxt "Sort icons"
#| msgid "By Type"
msgid "File Type"
msgstr "Nga Lloji"

#: package/contents/ui/ConfigFilter.qml:130
#, kde-format
msgid "Description"
msgstr ""

#: package/contents/ui/ConfigFilter.qml:199
#, kde-format
msgid "Select All"
msgstr "Zgjidhi të Gjitha"

#: package/contents/ui/ConfigFilter.qml:209
#, kde-format
msgid "Deselect All"
msgstr "Lësho Gjithçka"

#: package/contents/ui/ConfigFilter.qml:218
#, kde-format
msgid "Switch Sort Order"
msgstr ""

#: package/contents/ui/ConfigIcons.qml:61
#, kde-format
msgid "Panel button:"
msgstr ""

#: package/contents/ui/ConfigIcons.qml:67
#, kde-format
msgid "Use a custom icon"
msgstr ""

#: package/contents/ui/ConfigIcons.qml:100
#, fuzzy, kde-format
#| msgctxt "@title:menu in Copy To or Move To submenu"
#| msgid "Browse..."
msgctxt "@item:inmenu Open icon chooser dialog"
msgid "Choose…"
msgstr "Shfleto..."

#: package/contents/ui/ConfigIcons.qml:106
#, fuzzy, kde-format
#| msgid "Sort Icons"
msgctxt "@item:inmenu Reset icon to default"
msgid "Clear Icon"
msgstr "Rreshto Ikonat"

#: package/contents/ui/ConfigIcons.qml:126
#, kde-format
msgid "Arrangement:"
msgstr ""

#: package/contents/ui/ConfigIcons.qml:130
#, kde-format
msgctxt "@item:inlistbox arrangement of icons"
msgid "Left to Right"
msgstr ""

#: package/contents/ui/ConfigIcons.qml:131
#, kde-format
msgctxt "@item:inlistbox arrangement of icons"
msgid "Right to Left"
msgstr ""

#: package/contents/ui/ConfigIcons.qml:132
#, kde-format
msgctxt "@item:inlistbox arrangement of icons"
msgid "Top to Bottom"
msgstr ""

#: package/contents/ui/ConfigIcons.qml:141
#, fuzzy, kde-format
#| msgid "Align to Grid"
msgctxt "@item:inlistbox alignment of icons"
msgid "Align left"
msgstr "Vëri në Rresht"

#: package/contents/ui/ConfigIcons.qml:142
#, fuzzy, kde-format
#| msgid "Align to Grid"
msgctxt "@item:inlistbox alignment of icons"
msgid "Align right"
msgstr "Vëri në Rresht"

#: package/contents/ui/ConfigIcons.qml:157
#, fuzzy, kde-format
#| msgctxt "Icons on the desktop"
#| msgid "Lock in Place"
msgid "Lock in place"
msgstr "Blloko në Vend"

#: package/contents/ui/ConfigIcons.qml:171
#, kde-format
msgid "Sorting:"
msgstr ""

#: package/contents/ui/ConfigIcons.qml:179
#, kde-format
msgctxt "@item:inlistbox sort icons manually"
msgid "Manual"
msgstr ""

#: package/contents/ui/ConfigIcons.qml:180
#, fuzzy, kde-format
#| msgctxt "Sort icons"
#| msgid "By Name"
msgctxt "@item:inlistbox sort icons by name"
msgid "Name"
msgstr "Sipas emrit"

#: package/contents/ui/ConfigIcons.qml:181
#, fuzzy, kde-format
#| msgctxt "Sort icons"
#| msgid "By Size"
msgctxt "@item:inlistbox sort icons by size"
msgid "Size"
msgstr "Nga Përmasa"

#: package/contents/ui/ConfigIcons.qml:182
#, fuzzy, kde-format
#| msgctxt "Sort icons"
#| msgid "By Type"
msgctxt "@item:inlistbox sort icons by file type"
msgid "Type"
msgstr "Nga Lloji"

#: package/contents/ui/ConfigIcons.qml:183
#, fuzzy, kde-format
#| msgctxt "Sort icons"
#| msgid "By Date"
msgctxt "@item:inlistbox sort icons by date"
msgid "Date"
msgstr "Nga Data"

#: package/contents/ui/ConfigIcons.qml:194
#, kde-format
msgctxt "@option:check sort icons in descending order"
msgid "Descending"
msgstr ""

#: package/contents/ui/ConfigIcons.qml:202
#, fuzzy, kde-format
#| msgctxt "Sort icons"
#| msgid "Folders First"
msgctxt "@option:check sort icons with folders first"
msgid "Folders first"
msgstr "Kartelat së pari"

#: package/contents/ui/ConfigIcons.qml:216
#, kde-format
msgctxt "whether to use icon or list view"
msgid "View mode:"
msgstr ""

#: package/contents/ui/ConfigIcons.qml:218
#, kde-format
msgctxt "@item:inlistbox show icons in a list"
msgid "List"
msgstr ""

#: package/contents/ui/ConfigIcons.qml:219
#, kde-format
msgctxt "@item:inlistbox show icons in a grid"
msgid "Grid"
msgstr ""

#: package/contents/ui/ConfigIcons.qml:230
#, fuzzy, kde-format
#| msgctxt "Sort icons"
#| msgid "By Size"
msgid "Icon size:"
msgstr "Nga Përmasa"

#: package/contents/ui/ConfigIcons.qml:245
#, fuzzy, kde-format
#| msgid "Small"
msgctxt "@label:slider smallest icon size"
msgid "Small"
msgstr "E vogël"

#: package/contents/ui/ConfigIcons.qml:254
#, fuzzy, kde-format
#| msgid "Large"
msgctxt "@label:slider largest icon size"
msgid "Large"
msgstr "E Madhe"

#: package/contents/ui/ConfigIcons.qml:263
#, kde-format
msgid "Label width:"
msgstr ""

#: package/contents/ui/ConfigIcons.qml:266
#, kde-format
msgctxt "@item:inlistbox how long a text label should be"
msgid "Narrow"
msgstr ""

#: package/contents/ui/ConfigIcons.qml:267
#, kde-format
msgctxt "@item:inlistbox how long a text label should be"
msgid "Medium"
msgstr ""

#: package/contents/ui/ConfigIcons.qml:268
#, kde-format
msgctxt "@item:inlistbox how long a text label should be"
msgid "Wide"
msgstr ""

#: package/contents/ui/ConfigIcons.qml:276
#, kde-format
msgid "Text lines:"
msgstr ""

#: package/contents/ui/ConfigIcons.qml:292
#, kde-format
msgid "When hovering over icons:"
msgstr ""

#: package/contents/ui/ConfigIcons.qml:294
#, kde-format
msgid "Show tooltips"
msgstr ""

#: package/contents/ui/ConfigIcons.qml:301
#, fuzzy, kde-format
#| msgid "Select All"
msgid "Show selection markers"
msgstr "Zgjidhi të Gjitha"

#: package/contents/ui/ConfigIcons.qml:308
#, fuzzy, kde-format
#| msgid "Preview In"
msgid "Show folder preview popups"
msgstr "Shiko në"

#: package/contents/ui/ConfigIcons.qml:318
#, fuzzy, kde-format
#| msgid "&Rename"
msgid "Rename:"
msgstr "&Riemërto"

#: package/contents/ui/ConfigIcons.qml:322
#, kde-format
msgid "Rename inline by clicking selected item's text"
msgstr ""

#: package/contents/ui/ConfigIcons.qml:333
#, fuzzy, kde-format
#| msgid "Preview In"
msgid "Previews:"
msgstr "Shiko në"

#: package/contents/ui/ConfigIcons.qml:335
#, fuzzy, kde-format
#| msgid "Preview In"
msgid "Show preview thumbnails"
msgstr "Shiko në"

#: package/contents/ui/ConfigIcons.qml:343
#, kde-format
msgid "Configure Preview Plugins…"
msgstr ""

#: package/contents/ui/ConfigLocation.qml:80
#, kde-format
msgid "Show:"
msgstr ""

#: package/contents/ui/ConfigLocation.qml:82
#, fuzzy, kde-format
#| msgid "Select All"
msgid "Desktop folder"
msgstr "Zgjidhi të Gjitha"

#: package/contents/ui/ConfigLocation.qml:89
#, kde-format
msgid "Files linked to the current activity"
msgstr ""

#: package/contents/ui/ConfigLocation.qml:96
#, kde-format
msgid "Places panel item:"
msgstr ""

#: package/contents/ui/ConfigLocation.qml:129
#, fuzzy, kde-format
#| msgctxt ""
#| "Title of the page that lets the user choose which location should the "
#| "folderview show"
#| msgid "Location"
msgid "Custom location:"
msgstr "Vendndodhja"

#: package/contents/ui/ConfigLocation.qml:137
#, kde-format
msgid "Type path or URL…"
msgstr ""

#: package/contents/ui/ConfigLocation.qml:180
#, kde-format
msgid "Title:"
msgstr ""

#: package/contents/ui/ConfigLocation.qml:182
#, kde-format
msgid "None"
msgstr ""

#: package/contents/ui/ConfigLocation.qml:182
#, kde-format
msgid "Default"
msgstr ""

#: package/contents/ui/ConfigLocation.qml:182
#, kde-format
msgid "Full path"
msgstr ""

#: package/contents/ui/ConfigLocation.qml:182
#, kde-format
msgid "Custom title"
msgstr ""

#: package/contents/ui/ConfigLocation.qml:197
#, kde-format
msgid "Enter custom title…"
msgstr ""

#: package/contents/ui/ConfigOverlay.qml:86
#, kde-format
msgid "Click and drag to rotate"
msgstr ""

#: package/contents/ui/ConfigOverlay.qml:174
#, kde-format
msgid "Hide Background"
msgstr ""

#: package/contents/ui/ConfigOverlay.qml:174
#, kde-format
msgid "Show Background"
msgstr ""

#: package/contents/ui/ConfigOverlay.qml:222
#, kde-format
msgid "Remove"
msgstr ""

#: package/contents/ui/FolderItemPreviewPluginsDialog.qml:19
#, kde-format
msgid "Preview Plugins"
msgstr ""

#: package/contents/ui/FolderView.qml:1196
#, kde-format
msgctxt "@info"
msgid ""
"There are a lot of files and folders on the desktop. This can cause bugs and "
"performance issues. Please consider moving some of them elsewhere."
msgstr ""

#: package/contents/ui/main.qml:391
#, kde-format
msgid "Configure Desktop and Wallpaper…"
msgstr ""

#: plugins/folder/directorypicker.cpp:32
#, fuzzy, kde-format
#| msgid "Select All"
msgid "Select Folder"
msgstr "Zgjidhi të Gjitha"

#: plugins/folder/foldermodel.cpp:469
#, kde-format
msgid "&Refresh Desktop"
msgstr ""

#: plugins/folder/foldermodel.cpp:469 plugins/folder/foldermodel.cpp:1625
#, kde-format
msgid "&Refresh View"
msgstr ""

#: plugins/folder/foldermodel.cpp:1634
#, fuzzy, kde-format
#| msgid "&Empty Trash Bin"
msgid "&Empty Trash"
msgstr "&Zbraze Shportën"

#: plugins/folder/foldermodel.cpp:1637
#, fuzzy, kde-format
#| msgid "&Restore"
msgctxt "Restore from trash"
msgid "Restore"
msgstr "&Kthe"

#: plugins/folder/foldermodel.cpp:1640
#, kde-format
msgid "&Open"
msgstr "&Hap"

#: plugins/folder/foldermodel.cpp:1757
#, kde-format
msgid "&Paste"
msgstr "&Ngjit"

#: plugins/folder/foldermodel.cpp:1874
#, kde-format
msgid "&Properties"
msgstr "&Tiparet"

#: plugins/folder/viewpropertiesmenu.cpp:21
#, kde-format
msgid "Sort By"
msgstr ""

#: plugins/folder/viewpropertiesmenu.cpp:24
#, fuzzy, kde-format
#| msgctxt "Sort Icons"
#| msgid "Unsorted"
msgctxt "@item:inmenu Sort icons manually"
msgid "Unsorted"
msgstr "Pa renditur"

#: plugins/folder/viewpropertiesmenu.cpp:28
#, fuzzy, kde-format
#| msgctxt "Sort icons"
#| msgid "By Name"
msgctxt "@item:inmenu Sort icons by name"
msgid "Name"
msgstr "Sipas emrit"

#: plugins/folder/viewpropertiesmenu.cpp:32
#, fuzzy, kde-format
#| msgctxt "Sort icons"
#| msgid "By Size"
msgctxt "@item:inmenu Sort icons by size"
msgid "Size"
msgstr "Nga Përmasa"

#: plugins/folder/viewpropertiesmenu.cpp:36
#, fuzzy, kde-format
#| msgctxt "Sort icons"
#| msgid "By Type"
msgctxt "@item:inmenu Sort icons by file type"
msgid "Type"
msgstr "Nga Lloji"

#: plugins/folder/viewpropertiesmenu.cpp:40
#, fuzzy, kde-format
#| msgctxt "Sort icons"
#| msgid "By Date"
msgctxt "@item:inmenu Sort icons by date"
msgid "Date"
msgstr "Nga Data"

#: plugins/folder/viewpropertiesmenu.cpp:45
#, kde-format
msgctxt "@item:inmenu Sort icons in descending order"
msgid "Descending"
msgstr ""

#: plugins/folder/viewpropertiesmenu.cpp:47
#, fuzzy, kde-format
#| msgctxt "Sort icons"
#| msgid "Folders First"
msgctxt "@item:inmenu Sort icons with folders first"
msgid "Folders First"
msgstr "Kartelat së pari"

#: plugins/folder/viewpropertiesmenu.cpp:50
#, fuzzy, kde-format
#| msgctxt "Sort icons"
#| msgid "By Size"
msgid "Icon Size"
msgstr "Nga Përmasa"

#: plugins/folder/viewpropertiesmenu.cpp:53
#, kde-format
msgctxt "@item:inmenu size of the icons"
msgid "Tiny"
msgstr ""

#: plugins/folder/viewpropertiesmenu.cpp:54
#, fuzzy, kde-format
#| msgid "Small"
msgctxt "@item:inmenu size of the icons"
msgid "Very Small"
msgstr "E vogël"

#: plugins/folder/viewpropertiesmenu.cpp:55
#, fuzzy, kde-format
#| msgid "Small"
msgctxt "@item:inmenu size of the icons"
msgid "Small"
msgstr "E vogël"

#: plugins/folder/viewpropertiesmenu.cpp:56
#, kde-format
msgctxt "@item:inmenu size of the icons"
msgid "Small-Medium"
msgstr ""

#: plugins/folder/viewpropertiesmenu.cpp:57
#, kde-format
msgctxt "@item:inmenu size of the icons"
msgid "Medium"
msgstr ""

#: plugins/folder/viewpropertiesmenu.cpp:58
#, fuzzy, kde-format
#| msgid "Large"
msgctxt "@item:inmenu size of the icons"
msgid "Large"
msgstr "E Madhe"

#: plugins/folder/viewpropertiesmenu.cpp:59
#, kde-format
msgctxt "@item:inmenu size of the icons"
msgid "Huge"
msgstr ""

#: plugins/folder/viewpropertiesmenu.cpp:67
#, kde-format
msgctxt "@item:inmenu arrangement of icons"
msgid "Arrange"
msgstr ""

#: plugins/folder/viewpropertiesmenu.cpp:71
#, kde-format
msgctxt "@item:inmenu arrangement of icons"
msgid "Left to Right"
msgstr ""

#: plugins/folder/viewpropertiesmenu.cpp:72
#, kde-format
msgctxt "@item:inmenu arrangement of icons"
msgid "Right to Left"
msgstr ""

#: plugins/folder/viewpropertiesmenu.cpp:76
#, kde-format
msgctxt "@item:inmenu arrangement of icons"
msgid "Top to Bottom"
msgstr ""

#: plugins/folder/viewpropertiesmenu.cpp:81
#, fuzzy, kde-format
#| msgid "Align to Grid"
msgid "Align"
msgstr "Vëri në Rresht"

#: plugins/folder/viewpropertiesmenu.cpp:84
#, kde-format
msgctxt "@item:inmenu alignment of icons"
msgid "Left"
msgstr ""

#: plugins/folder/viewpropertiesmenu.cpp:88
#, kde-format
msgctxt "@item:inmenu alignment of icons"
msgid "Right"
msgstr ""

#: plugins/folder/viewpropertiesmenu.cpp:93
#, fuzzy, kde-format
#| msgid "Preview In"
msgid "Show Previews"
msgstr "Shiko në"

#: plugins/folder/viewpropertiesmenu.cpp:97
#, kde-format
msgctxt "@item:inmenu lock icon positions in place"
msgid "Locked"
msgstr ""

#, fuzzy
#~| msgid "Create &Folder..."
#~ msgid "&Create Folder"
#~ msgstr "Krijo &Kartelë"

#, fuzzy
#~| msgctxt "Sort icons"
#~| msgid "By Size"
#~ msgid "Size:"
#~ msgstr "Nga Përmasa"

#~ msgid "&Reload"
#~ msgstr "&Ringarko"

#~ msgid "&Move to Trash"
#~ msgstr "&Hidhe tek Koshi i Mbeturinave"

#~ msgid "&Delete"
#~ msgstr "&Fshije"

#, fuzzy
#~| msgid "Align to Grid"
#~ msgid "Align:"
#~ msgstr "Vëri në Rresht"

#~ msgid "Show Original Directory"
#~ msgstr "Shfaq Direktorinë origjinale"

#, fuzzy
#~| msgid "&Empty Trash Bin"
#~ msgid "&Configure Trash Bin"
#~ msgstr "&Zbraze Shportën"

#~ msgid "&Bookmark This Page"
#~ msgstr "&Shëno Këtë Faqe"

#~ msgid "&Bookmark This Location"
#~ msgstr "&Shëno Këtë Lokacion"

#~ msgid "&Bookmark This Folder"
#~ msgstr "&Shëno Këtë Kartelë"

#~ msgid "&Bookmark This Link"
#~ msgstr "&Shëno Këtë Link"

#~ msgid "&Bookmark This File"
#~ msgstr "&Shëno Këtë Skedë"

#~ msgctxt "@title:menu"
#~ msgid "Copy To"
#~ msgstr "Kopjo në"

#~ msgctxt "@title:menu"
#~ msgid "Move To"
#~ msgstr "Lëviz në"

#~ msgctxt "@title:menu"
#~ msgid "Home Folder"
#~ msgstr "Kartela e Shtëpisë"

#~ msgctxt "@title:menu"
#~ msgid "Root Folder"
#~ msgstr "Follderi Bazë"

#~ msgctxt "@title:menu in Copy To or Move To submenu"
#~ msgid "Browse..."
#~ msgstr "Shfleto..."

#~ msgctxt "@title:menu"
#~ msgid "Copy Here"
#~ msgstr "Kopjo Këtu"

#~ msgctxt "@title:menu"
#~ msgid "Move Here"
#~ msgstr "Lëviz Këtu"

#~ msgid "Share"
#~ msgstr "Ndaje"

#, fuzzy
#~| msgid "Icons"
#~ msgid "Icon:"
#~ msgstr "Ikonat"

#~ msgid "You cannot drop a folder on to itself"
#~ msgstr "Ju nuk mund ta vendosni një skedë në vetëvete"

#~ msgid "File name for dropped contents:"
#~ msgstr "Emri për përmbajtjet që po vendosen:"

#~ msgid "&Move Here"
#~ msgstr "Lëvize &këtu"

#~ msgid "&Copy Here"
#~ msgstr "Kopjo kë&tu"

#~ msgid "&Link Here"
#~ msgstr "&Ndërlidh këtu"

#~ msgid "C&ancel"
#~ msgstr "Anulo"

#~ msgid "Set as &Wallpaper"
#~ msgstr "Përdor si &sfond"

#~ msgctxt "@action:inmenu"
#~ msgid "Paste One Folder"
#~ msgstr "Ngjit Një Dosje"

#~ msgctxt "@action:inmenu"
#~ msgid "Paste One File"
#~ msgstr "Ngjit Një Skedar"

#~ msgctxt "@action:inmenu"
#~ msgid "Paste Clipboard Contents..."
#~ msgstr "Ngjit Përmbajtjen e Kujtesës..."

#, fuzzy
#~| msgid "&Paste"
#~ msgctxt "@action:inmenu"
#~ msgid "Paste"
#~ msgstr "&Ngjit"

#~ msgid ""
#~ "Restores this file or directory, back to the location where it was "
#~ "deleted from initially"
#~ msgstr ""
#~ "Rikthen skedarin apo direktorinë në vendin nga ku ai ishte fshirë më parë"

#~ msgid ""
#~ "Opens a new file manager window showing the target of this link, in its "
#~ "parent directory."
#~ msgstr ""
#~ "Hap një dritare të re të menaxhuesit të skedarëve e cila tregon "
#~ "shënjestrën e kësaj lidhjeje, në direktorinë e saj prind."

#, fuzzy
#~| msgid "Icons"
#~ msgctxt ""
#~ "Title of the page that lets the user choose how the folderview should be "
#~ "shown"
#~ msgid "Icons"
#~ msgstr "Ikonat"

#~ msgid "Align to Grid"
#~ msgstr "Vëri në Rresht"

#~ msgctxt "Icons on the desktop"
#~ msgid "Lock in Place"
#~ msgstr "Blloko në Vend"

#~ msgctxt "%1 and %2 are the messages translated above."
#~ msgid "%1, %2."
#~ msgstr "%1, %2."

#, fuzzy
#~| msgid "Align to Grid"
#~ msgid "Align to grid:"
#~ msgstr "Vëri në Rresht"

#~ msgctxt ""
#~ "Title of the page that lets the user choose how the folderview should be "
#~ "shown"
#~ msgid "Display"
#~ msgstr "Ekrani"
