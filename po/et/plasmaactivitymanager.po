# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Marek Laane <qiilaq69@gmail.com>, 2016.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-08 00:48+0000\n"
"PO-Revision-Date: 2016-08-20 03:06+0300\n"
"Last-Translator: Marek Laane <qiilaq69@gmail.com>\n"
"Language-Team: Estonian <kde-i18n-doc@kde.org>\n"
"Language: et\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 1.5\n"

#: sortedactivitiesmodel.cpp:311
#, kde-format
msgid "Used some time ago"
msgstr "Kasutati mõne aja eest"

#: sortedactivitiesmodel.cpp:327
#, kde-format
msgid "Used more than a year ago"
msgstr "Kasutati üle aasta tagasi"

#: sortedactivitiesmodel.cpp:328
#, kde-format
msgctxt "amount in months"
msgid "Used a month ago"
msgid_plural "Used %1 months ago"
msgstr[0] "Kasutati kuu eest"
msgstr[1] "Kasutati %1 kuu eest"

#: sortedactivitiesmodel.cpp:329
#, kde-format
msgctxt "amount in days"
msgid "Used a day ago"
msgid_plural "Used %1 days ago"
msgstr[0] "Kasutati päeva eest"
msgstr[1] "Kasutati %1 päeva eest"

#: sortedactivitiesmodel.cpp:330
#, kde-format
msgctxt "amount in hours"
msgid "Used an hour ago"
msgid_plural "Used %1 hours ago"
msgstr[0] "Kasutati tunni eest"
msgstr[1] "Kasutati %1 tunni eest"

#: sortedactivitiesmodel.cpp:331
#, kde-format
msgctxt "amount in minutes"
msgid "Used a minute ago"
msgid_plural "Used %1 minutes ago"
msgstr[0] "Kasutati minuti eest"
msgstr[1] "Kasutati %1 minuti eest"

#: sortedactivitiesmodel.cpp:332
#, kde-format
msgid "Used a moment ago"
msgstr "Kasutati äsja"

#: switcherbackend.cpp:167
#, kde-format
msgid "Walk through activities"
msgstr "Keri läbi tegevuste"

#: switcherbackend.cpp:172
#, kde-format
msgid "Walk through activities (Reverse)"
msgstr "Keri läbi tegevuste (teistpidi)"
