# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-26 02:00+0000\n"
"PO-Revision-Date: 2023-05-19 10:21+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.1\n"

#: contents/activitymanager/ActivityItem.qml:205
msgid "Currently being used"
msgstr "Wordt nu gebruikt"

#: contents/activitymanager/ActivityItem.qml:245
msgid ""
"Move to\n"
"this activity"
msgstr ""
"Verplaatsen naar\n"
"deze activiteit"

#: contents/activitymanager/ActivityItem.qml:275
msgid ""
"Show also\n"
"in this activity"
msgstr ""
"Ook tonen in\n"
"deze activiteit"

#: contents/activitymanager/ActivityItem.qml:337
msgid "Configure"
msgstr "Configureren"

#: contents/activitymanager/ActivityItem.qml:356
msgid "Stop activity"
msgstr "Activiteit stoppen"

#: contents/activitymanager/ActivityList.qml:142
msgid "Stopped activities:"
msgstr "Gestopte activiteiten"

#: contents/activitymanager/ActivityManager.qml:120
msgid "Create activity…"
msgstr "Activiteit aanmaken…"

#: contents/activitymanager/Heading.qml:59
msgid "Activities"
msgstr "Activiteiten"

#: contents/activitymanager/StoppedActivityItem.qml:137
msgid "Configure activity"
msgstr "Activiteit instellen"

#: contents/activitymanager/StoppedActivityItem.qml:154
msgid "Delete"
msgstr "Verwijderen"

#: contents/applet/AppletError.qml:128
msgid "Sorry! There was an error loading %1."
msgstr "Sorry! Er was een fout bij laden van %1."

#: contents/applet/AppletError.qml:166
msgid "Copy to Clipboard"
msgstr "Naar klembord kopiëren"

#: contents/applet/AppletError.qml:189
msgid "View Error Details…"
msgstr "Foutdetails bekijken…"

#: contents/applet/CompactApplet.qml:74
msgid "Open %1"
msgstr "%1 openen"

#: contents/configuration/AboutPlugin.qml:20
#: contents/configuration/AppletConfiguration.qml:245
msgid "About"
msgstr "Info over"

#: contents/configuration/AboutPlugin.qml:48
msgid "Send an email to %1"
msgstr "Een e-mail sturen naar %1"

#: contents/configuration/AboutPlugin.qml:62
msgctxt "@info:tooltip %1 url"
msgid "Open website %1"
msgstr "Website %1 openen"

#: contents/configuration/AboutPlugin.qml:130
msgid "Copyright"
msgstr "Copyright"

#: contents/configuration/AboutPlugin.qml:148 contents/explorer/Tooltip.qml:92
msgid "License:"
msgstr "Licentie:"

#: contents/configuration/AboutPlugin.qml:151
msgctxt "@info:whatsthis"
msgid "View license text"
msgstr "Licentietekst bekijken"

#: contents/configuration/AboutPlugin.qml:165
msgid "Authors"
msgstr "Auteurs"

#: contents/configuration/AboutPlugin.qml:176
msgid "Credits"
msgstr "Dankbetuigingen"

#: contents/configuration/AboutPlugin.qml:188
msgid "Translators"
msgstr "Vertalers"

#: contents/configuration/AboutPlugin.qml:205
msgid "Report a Bug…"
msgstr "Een bug rapporteren…"

#: contents/configuration/AppletConfiguration.qml:56
msgid "Keyboard Shortcuts"
msgstr "Sneltoetsen"

#: contents/configuration/AppletConfiguration.qml:293
msgid "Apply Settings"
msgstr "Instellingen toepassen"

#: contents/configuration/AppletConfiguration.qml:294
msgid ""
"The settings of the current module have changed. Do you want to apply the "
"changes or discard them?"
msgstr ""
"De instellingen van het huidige module zijn gewijzigd. Wilt u de wijzigingen "
"toepassen of verwerpen?"

#: contents/configuration/AppletConfiguration.qml:324
msgid "OK"
msgstr "OK"

#: contents/configuration/AppletConfiguration.qml:332
msgid "Apply"
msgstr "Toepassen"

#: contents/configuration/AppletConfiguration.qml:338
msgid "Cancel"
msgstr "Annuleren"

#: contents/configuration/ConfigCategoryDelegate.qml:27
msgid "Open configuration page"
msgstr "Configuratiepagina openen"

#: contents/configuration/ConfigurationContainmentActions.qml:21
msgid "Left-Button"
msgstr "Linker muisknop"

#: contents/configuration/ConfigurationContainmentActions.qml:22
msgid "Right-Button"
msgstr "Rechter muisknop"

#: contents/configuration/ConfigurationContainmentActions.qml:23
msgid "Middle-Button"
msgstr "Middelste muisknop"

#: contents/configuration/ConfigurationContainmentActions.qml:24
msgid "Back-Button"
msgstr "Knop voor terug"

#: contents/configuration/ConfigurationContainmentActions.qml:25
msgid "Forward-Button"
msgstr "Knop voor voorwaarts"

#: contents/configuration/ConfigurationContainmentActions.qml:27
msgid "Vertical-Scroll"
msgstr "Verticaal schuiven"

#: contents/configuration/ConfigurationContainmentActions.qml:28
msgid "Horizontal-Scroll"
msgstr "Horizontaal schuiven"

#: contents/configuration/ConfigurationContainmentActions.qml:30
msgid "Shift"
msgstr "Shift"

#: contents/configuration/ConfigurationContainmentActions.qml:31
msgid "Ctrl"
msgstr "Ctrl"

#: contents/configuration/ConfigurationContainmentActions.qml:32
msgid "Alt"
msgstr "Alt"

#: contents/configuration/ConfigurationContainmentActions.qml:33
msgid "Meta"
msgstr "Meta"

#: contents/configuration/ConfigurationContainmentActions.qml:98
msgctxt "Concatenation sign for shortcuts, e.g. Ctrl+Shift"
msgid "+"
msgstr "+"

#: contents/configuration/ConfigurationContainmentActions.qml:170
msgctxt "@title"
msgid "About"
msgstr "Info over"

#: contents/configuration/ConfigurationContainmentActions.qml:185
#: contents/configuration/MouseEventInputButton.qml:13
msgid "Add Action"
msgstr "Actie toevoegen"

#: contents/configuration/ConfigurationContainmentAppearance.qml:67
msgid "Layout changes have been restricted by the system administrator"
msgstr "Wijzigingen in de indeling zijn beperkt door de systeembeheerder"

#: contents/configuration/ConfigurationContainmentAppearance.qml:82
msgid "Layout:"
msgstr "Indeling:"

#: contents/configuration/ConfigurationContainmentAppearance.qml:96
msgid "Wallpaper type:"
msgstr "Type achtergrondafbeelding:"

#: contents/configuration/ConfigurationContainmentAppearance.qml:116
msgid "Get New Plugins…"
msgstr "Haal nieuwe plug-ins op…"

#: contents/configuration/ConfigurationContainmentAppearance.qml:184
msgid "Layout changes must be applied before other changes can be made"
msgstr ""
"Wijzigingen aan indeling moeten worden toegepast alvorens andere wijzigingen "
"gemaakt kunnen worden"

#: contents/configuration/ConfigurationContainmentAppearance.qml:188
msgid "Apply Now"
msgstr "Nu toepassen"

#: contents/configuration/ConfigurationShortcuts.qml:17
msgid "Shortcuts"
msgstr "Sneltoetsen"

#: contents/configuration/ConfigurationShortcuts.qml:29
msgid "This shortcut will activate the applet as though it had been clicked."
msgstr "Deze sneltoets zal de applet activeren alsof er op is geklikt."

#: contents/configuration/ContainmentConfiguration.qml:29
msgid "Wallpaper"
msgstr "Achtergrondafbeelding"

#: contents/configuration/ContainmentConfiguration.qml:34
msgid "Mouse Actions"
msgstr "Muisacties"

#: contents/configuration/MouseEventInputButton.qml:20
msgid "Input Here"
msgstr "Invoer hier"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:43
msgid "Panel Settings"
msgstr "Paneelinstellingen"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:49
msgctxt "@action:button Make the panel as big as it can be"
msgid "Maximize"
msgstr "Maximaliseren"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:53
msgid "Make this panel as tall as possible"
msgstr "Maakt dit paneel zo hoog als mogelijk is."

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:54
msgid "Make this panel as wide as possible"
msgstr "Maakt dit paneel zo breed als mogelijk is."

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:62
msgctxt "@action:button Delete the panel"
msgid "Delete"
msgstr "Verwijderen"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:65
msgid "Remove this panel; this action is undo-able"
msgstr "Dit paneel verwijderen; deze actie kan niet ongedaan worden"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:89
msgid "Alignment:"
msgstr "Uitlijning:"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:95
msgid "Top"
msgstr "Bovenaan"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:95
msgid "Left"
msgstr "Links"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:96
msgid ""
"Aligns a non-maximized panel to the top; no effect when panel is maximized"
msgstr ""
"Lijnt een niet gemaximaliseerd paneel bovenaan uit; geen effect wanneer het "
"paneel gemaximaliseerd is"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:96
msgid ""
"Aligns a non-maximized panel to the left; no effect when panel is maximized"
msgstr ""
"Lijnt een niet gemaximaliseerd paneel links uit; geen effect wanneer het "
"paneel gemaximaliseerd is"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:104
msgid "Center"
msgstr "Midden"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:105
msgid ""
"Aligns a non-maximized panel to the center; no effect when panel is maximized"
msgstr ""
"Lijnt een niet gemaximaliseerd paneel centraal uit; geen effect wanneer het "
"paneel gemaximaliseerd is"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:113
msgid "Bottom"
msgstr "Onderaan"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:113
msgid "Right"
msgstr "Rechts"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:114
msgid ""
"Aligns a non-maximized panel to the bottom; no effect when panel is maximized"
msgstr ""
"Lijnt een niet gemaximaliseerd paneel onderaan uit; geen effect wanneer het "
"paneel gemaximaliseerd is"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:114
msgid ""
"Aligns a non-maximized panel to the right; no effect when panel is maximized"
msgstr ""
"Lijnt een niet gemaximaliseerd paneel rechts uit; geen effect wanneer het "
"paneel gemaximaliseerd is"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:142
msgid "Visibility:"
msgstr "Zichtbaarheid:"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:147
msgid "Always Visible"
msgstr "Altijd zichtbaar"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:155
msgid "Auto-Hide"
msgstr "Automatisch verbergen"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:156
msgid ""
"Panel is hidden, but reveals itself when the cursor touches the panel's "
"screen edge"
msgstr ""
"Paneel is verborgen, maar laat zich zien wanneer de cursor de schermrand van "
"het paneel raakt"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:183
msgid "Opacity:"
msgstr "Dekking:"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:188
msgid "Always Opaque"
msgstr "Altijd dekkend"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:196
msgid "Adaptive"
msgstr "Past zich aan"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:197
msgid ""
"Panel is opaque when any windows are touching it, and translucent at other "
"times"
msgstr ""
"Panel is dekkend wanneer vensters het raken en transparant op andere momenten"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:206
msgid "Always Translucent"
msgstr "Altijd transparant"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:233
msgid "Floating:"
msgstr "Zwevend:"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:238
msgid "Floating"
msgstr "Zwevend"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:239
msgid "Panel visibly floats away from its screen edge"
msgstr "Panel visibly floats away from its screen edge"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:248
msgid "Attached"
msgstr "Vastgemaakt"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:249
msgid "Panel is attached to its screen edge"
msgstr "Paneel is vastgezet aan zijn schermrand"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:271
msgid "Focus Shortcut:"
msgstr "Sneltoets voor focus:"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:281
msgid "Press this keyboard shortcut to move focus to the Panel"
msgstr "Deze sneltoets indrukken om focus te verplaatsen naar het paneel"

#: contents/configuration/panelconfiguration/Ruler.qml:20
msgid "Drag to change maximum height."
msgstr "Slepen om de maximale hoogte te wijzigen."

#: contents/configuration/panelconfiguration/Ruler.qml:20
msgid "Drag to change maximum width."
msgstr "Slepen om de maximale breedte te wijzigen."

#: contents/configuration/panelconfiguration/Ruler.qml:20
#: contents/configuration/panelconfiguration/Ruler.qml:21
msgid "Double click to reset."
msgstr "Dubbelklik om te resetten."

#: contents/configuration/panelconfiguration/Ruler.qml:21
msgid "Drag to change minimum height."
msgstr "Slepen om de minimale hoogte te wijzigen."

#: contents/configuration/panelconfiguration/Ruler.qml:21
msgid "Drag to change minimum width."
msgstr "Slepen om de minimale breedte te wijzigen."

#: contents/configuration/panelconfiguration/Ruler.qml:65
msgid ""
"Drag to change position on this screen edge.\n"
"Double click to reset."
msgstr ""
"Slepen on de positie op deze schermrand te wijzigen.\n"
"Dubbelklik om te resetten."

#: contents/configuration/panelconfiguration/ToolBar.qml:26
msgid "Add Widgets…"
msgstr "Widgets toevoegen…"

#: contents/configuration/panelconfiguration/ToolBar.qml:27
msgid "Add Spacer"
msgstr "Paneelscheider toevoegen"

#: contents/configuration/panelconfiguration/ToolBar.qml:28
msgid "More Options…"
msgstr "Meer opties…"

#: contents/configuration/panelconfiguration/ToolBar.qml:226
msgctxt "Minimize the length of this string as much as possible"
msgid "Drag to move"
msgstr "Slepen om te verplaatsen"

#: contents/configuration/panelconfiguration/ToolBar.qml:265
msgctxt "@info:tooltip"
msgid "Use arrow keys to move the panel"
msgstr "Pijltjestoetsen gebruiken om het paneel te verplaatsen"

#: contents/configuration/panelconfiguration/ToolBar.qml:286
msgid "Panel width:"
msgstr "Paneelbreedte:"

#: contents/configuration/panelconfiguration/ToolBar.qml:286
msgid "Panel height:"
msgstr "Paneelhoogte:"

#: contents/configuration/panelconfiguration/ToolBar.qml:406
#: contents/configuration/ShellContainmentConfiguration.qml:43
msgid "Close"
msgstr "Sluiten"

#: contents/configuration/ShellContainmentConfiguration.qml:19
msgid "Panels and Desktops Management"
msgstr "Beheer van panelen en bureaublad"

#: contents/configuration/ShellContainmentConfiguration.qml:34
msgid ""
"You can drag Panels and Desktops around to move them to different screens."
msgstr ""
"U kunt Panelen en Bureaubladen slepen om ze naar andere schermen te "
"verplaatsen."

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:178
msgid "Swap with Desktop on Screen %1"
msgstr "Omwisselen met Bureaublad op Scherm %1"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:179
msgid "Move to Screen %1"
msgstr "Naar scherm %1 verplaatsen"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:193
msgid "Remove Desktop"
msgstr "Bureaublad verwijderen"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:194
msgid "Remove Panel"
msgstr "Paneel verwijderen"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:267
msgid "%1 (primary)"
msgstr "%1 (eerste)"

#: contents/explorer/AppletAlternatives.qml:64
msgid "Alternative Widgets"
msgstr "Alternatieve widgets"

#: contents/explorer/AppletDelegate.qml:167
msgid "Undo uninstall"
msgstr "Deïnstalleren ongedaan maken"

#: contents/explorer/AppletDelegate.qml:168
msgid "Uninstall widget"
msgstr "Widget voor deïnstalleren"

#: contents/explorer/Tooltip.qml:101
msgid "Author:"
msgstr "Auteur:"

#: contents/explorer/Tooltip.qml:109
msgid "Email:"
msgstr "E-mail:"

#: contents/explorer/Tooltip.qml:128
msgid "Uninstall"
msgstr "Deïnstalleren"

#: contents/explorer/WidgetExplorer.qml:118
#: contents/explorer/WidgetExplorer.qml:192
msgid "All Widgets"
msgstr "Alle widgets"

#: contents/explorer/WidgetExplorer.qml:144
msgid "Widgets"
msgstr "Widgets"

#: contents/explorer/WidgetExplorer.qml:152
msgid "Get New Widgets…"
msgstr "Nieuwe widgets ophalen…"

#: contents/explorer/WidgetExplorer.qml:203
msgid "Categories"
msgstr "Categorieën"

#: contents/explorer/WidgetExplorer.qml:283
msgid "No widgets matched the search terms"
msgstr "Geen widgets komen overeen met de zoektermen"

#: contents/explorer/WidgetExplorer.qml:283
msgid "No widgets available"
msgstr "Geen widgets beschikbaar"

#~ msgid "Switch"
#~ msgstr "Wisselen"

#~| msgid "Windows Behind"
#~ msgid "Windows Above"
#~ msgstr "Vensters er boven"

#~ msgid ""
#~ "Like \"Auto-Hide\" but the panel remains visible as long as no windows "
#~ "are covering it up"
#~ msgstr ""
#~ "Net als \"Automatisch verbergen\" maar het paneel blijft zichtbaar zolang "
#~ "geen vensters het bedekken"

#~| msgid "Windows Behind"
#~ msgid "Windows Below"
#~ msgstr "Vensters er onder"

#~ msgid ""
#~ "Like \"Always Visible\", but maximized and tiled windows go under the "
#~ "panel as though it didn't exist"
#~ msgstr ""
#~ "Net als \"Altijd zichtbaar\", maar gemaximaliseerd en vensters schuin "
#~ "achter elkaar gaan onder het paneel alsof ze niet bestaan"

#~ msgid "Windows In Front"
#~ msgstr "Vensters vooraan"

#~ msgid "Aligns the panel"
#~ msgstr "Lijnt het paneel uit"

#~ msgid "Center aligns the panel if the panel is not maximized."
#~ msgstr "Centreert het paneel als het paneel niet gemaximaliseerd is."

#~ msgid ""
#~ "Makes the panel hidden always but reveals it when mouse enters the area "
#~ "where the panel would have been if it were not hidden."
#~ msgstr ""
#~ "Maakt het paneel altijd verborgen maar maakt het zichtbaar wanneer de "
#~ "muis het gebied ingaat waar het paneel zou zijn als het niet verborgen "
#~ "zou zijn."

#~ msgid ""
#~ "Makes the panel remain visible always but maximized windows shall cover "
#~ "it. It is revealed when mouse enters the area where the panel would have "
#~ "been if it were not covered."
#~ msgstr ""
#~ "Laat het paneel altijd zichtbaar blijven maar het gemaximaliseerde "
#~ "venster zal het bedekken. Het wordt zichtbaar wanneer de muis het gebied "
#~ "ingaat waar het paneel zou zijn als het niet was bedekt."

#~ msgid "Makes the panel translucent except when some windows touch it."
#~ msgstr "Maakt het paneel doorzichtig behalve wanneer een venster het raakt."

#~ msgid "Makes the panel translucent always."
#~ msgstr "Maakt het paneel altijd doorzichtig."

#~ msgid "Makes the panel float from the edge of the screen."
#~ msgstr "Laat het paneel zweven van de rand van het scherm."

#~ msgid "Makes the panel remain attached to the edge of the screen."
#~ msgstr "Laat het paneel vastgemaakt aan de rand van het scherm."

#~ msgid "%1 (disabled)"
#~ msgstr "%1 (uitgeschakeld)"

#~ msgid "Appearance"
#~ msgstr "Uiterlijk"

#~ msgid "Search…"
#~ msgstr "Zoeken…"

#~ msgid "Screen Edge"
#~ msgstr "Schermrand"

#~ msgid "Click and drag the button to a screen edge to move the panel there."
#~ msgstr ""
#~ "Klik en sleep de knop naar een schermrand om het paneel daarheen te "
#~ "verplaatsen."

#~ msgid "Width"
#~ msgstr "Breedte"

#~ msgid "Height"
#~ msgstr "Hoogte"

#~ msgid "Click and drag the button to resize the panel."
#~ msgstr "Klik en sleep de knop om het paneel van grootte te wijzigen."

#~ msgid "Layout cannot be changed while widgets are locked"
#~ msgstr "Indeling kan niet gewijzigd worden wanneer widgets vergrendeld zijn"

#~ msgid "Lock Widgets"
#~ msgstr "Widgets vergrendelen"

#~ msgid ""
#~ "This shortcut will activate the applet: it will give the keyboard focus "
#~ "to it, and if the applet has a popup (such as the start menu), the popup "
#~ "will be open."
#~ msgstr ""
#~ "Deze sneltoets zal de applet activeren: het verlegt de focus van het "
#~ "toetsenbord naar deze applet en als deze applet een pop-up heeft (zoals "
#~ "het startmenu), zal de pop-up geopend worden."

#~ msgid "Stop"
#~ msgstr "Stoppen"

#~ msgid "Activity name:"
#~ msgstr "Activiteitnaam:"

#~ msgid "Create"
#~ msgstr "Aanmaken"

#~ msgid "Are you sure you want to delete this activity?"
#~ msgstr "Wilt u deze activiteit verwijderen?"

#~ msgctxt "org.kde.plasma.desktop"
#~ msgid "Ok"
#~ msgstr "OK"

#~ msgctxt "org.kde.plasma.desktop"
#~ msgid "Apply"
#~ msgstr "Toepassen"

#~ msgctxt "org.kde.plasma.desktop"
#~ msgid "Cancel"
#~ msgstr "Annuleren"
